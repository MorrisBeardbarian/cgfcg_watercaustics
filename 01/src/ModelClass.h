#pragma once
#include <d3d11.h>
#include <fstream>
#include <DirectXColors.h>
#include "BaseExample.h"
#include "ShaderProgram.h"
#include "ConstantBuffers.h"
#include "ShaderProgram.h"
#include "Text.h"
#include "TexturingExample.h"
#include "RectangularPlane.h"

using namespace DirectX;


namespace WaterCaustics {
	class ModelClass {
	private:
		struct VertexType
		{
			public:
			XMFLOAT3 position;
			XMFLOAT2 texture;
			XMFLOAT3 normal;
		};

		struct ModelType
		{
		public:
			float x, y, z;
			float tu, tv;
			float nx, ny, nz;
		};

	protected:
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int m_vertexCount, m_indexCount;
		ID3D11ShaderResourceView* texture;
		ModelType* model;

	public:
		ModelClass(ID3D11Device* device, WCHAR* textureFilename, char* modelFilename);
		bool LoadWater(char* modelFilename);
		bool InitializeBuffers(ID3D11Device* device);
		bool LoadWaterTexture(ID3D11Device* device, WCHAR* filename);
		void Render(ID3D11DeviceContext* deviceContext);
		void RenderBuffers(ID3D11DeviceContext* deviceContext);

		int GetIndexCount();
		ID3D11ShaderResourceView* GetTexture();
	};
}