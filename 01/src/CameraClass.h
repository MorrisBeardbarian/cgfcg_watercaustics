#pragma once
#include <d3d11.h>
#include <fstream>
#include <DirectXColors.h>
#include "BaseExample.h"
#include "ShaderProgram.h"
#include "ConstantBuffers.h"
#include "ShaderProgram.h"
#include "Text.h"
#include "TexturingExample.h"
#include "RectangularPlane.h"

using namespace DirectX;

class CameraClass
{
public:
	CameraClass();
	CameraClass(const CameraClass&);
	~CameraClass();

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);

	void Render();
	void GetViewMatrix(XMMATRIX&);

	void RenderReflection(float);
	XMMATRIX GetReflectionViewMatrix();

private:
	XMMATRIX m_viewMatrix;
	float m_positionX, m_positionY, m_positionZ;
	float m_rotationX, m_rotationY, m_rotationZ;
	XMMATRIX m_reflectionViewMatrix;
};