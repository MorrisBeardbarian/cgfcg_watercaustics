#pragma once
#include "BaseExample.h"
#include "ShaderProgram.h"
#include "ConstantBuffers.h"
#include "ShaderProgram.h"
#include "Text.h"
#include "TexturingExample.h"
#include "RectangularPlane.h"
#include "ModelClass.h"
#include "WaterShaderClass.h"
#include "RefractionShader.h"
#include "RenderTexture.h"

namespace WaterCaustics {
	class WaterCausticsShadingExample : public BaseExample
	{
	protected:
		using TextureShader = ShaderProgram<Texturing::ConstantBuffer>;

		ID3D11ShaderResourceView* groundFloorTexture_ = nullptr;
		ID3D11SamplerState* textureSampler_ = nullptr;

		// std::unique_ptr<PhongShader> phongShader_;
		Shaders::PSolidShader solidShader_;
		std::unique_ptr<TextureShader> texturedPhong_;
		std::unique_ptr<RectangularPlane> groundPlane_;
		std::unique_ptr<Text::Text> infoText_;

		float waterTranslation=0, waterHeight = 5.0f;
		RenderTexture* refractionTexture;
		RenderTexture* reflectionTexture;
		ModelClass* waterModel;
		ModelClass* wallModel;
		ModelClass* bathModel;
		RefractionShader* refractionShader;
		WaterShaderClass* waterShader;
		XMMATRIX worldMatrix_;

		HRESULT setup() override;
		bool reloadShadersInternal() override;
		void render() override;
		void handleInput() override;

	public:
		virtual ~WaterCausticsShadingExample() = default;
	};
}
