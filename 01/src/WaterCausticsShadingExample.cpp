#include "WaterCausticsShadingExample.h"
#include "WinKeyMap.h"
#include <DirectXColors.h>

using namespace DirectX;

namespace WaterCaustics {

// ===========================================
// Set up scene
// - Plane (representing ocean floor)
// - Plane (simulating water surface)
// (- additional light sources)
// ===========================================
HRESULT WaterCausticsShadingExample::setup()
{
	auto hr = BaseExample::setup();
	if (FAILED(hr)) return hr;

	hr = reloadShaders();
	if (FAILED(hr)) return hr;

	groundPlane_ = std::make_unique<RectangularPlane>(context_.d3dDevice_);
	solidShader_ = Shaders::createSolidShader(context_);

	//Morris commented this
	// Create floor texture (for now I'm following the TexturingExample
	// to see if it works
	//hr = CreateDDSTextureFromFile(context_.d3dDevice_, L"textures/groundFloorTextureR.dds", true, nullptr, &groundFloorTexture_);
	//hr = CreateDDSTextureFromFile(context_.d3dDevice_, L"models/Water/water01.dds", true, nullptr, &groundFloorTexture_);
	//if (FAILED(hr))
	//	return hr;

	// Create the wall model object. context_.d3dDevice_, (WCHAR *)L"models/Water/water01.dds", (char *)"models/Water/water.txt"
	wallModel = new ModelClass(context_.d3dDevice_, (WCHAR *)L"models/wall/wall01.dds", (char *)"models/wall/wall.txt");

	// Create the bath model object.
	bathModel = new ModelClass(context_.d3dDevice_, (WCHAR *)L"models/bath/marble01.dds", (char *)"models/bath/bath.txt");

	// ======================
	// Create texture sampler
	// ======================
	//Morris commented this
	/*
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	*/

	/* Uncomment following to enable point filtering */
	//sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;

	/* Uncomment following to enable anisotropic filtering */
	//sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	//sampDesc.MaxAnisotropy = 16;

	//Morris commented this
	/*
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = context_.d3dDevice_->CreateSamplerState(&sampDesc, &textureSampler_);
	if (FAILED(hr))
		return hr;
	*/
	// Create info text with hint to render on screen
	infoText_ = std::make_unique<Text::Text>(context_.d3dDevice_, context_.immediateContext_, "Move with WASD");


	// Prepare Water
	waterModel = new ModelClass(context_.d3dDevice_, (WCHAR *)L"models/Water/water01.dds", (char *)"models/Water/water.txt");

	bool result = true;

	// Create the refraction render to texture object.
	refractionTexture = new RenderTexture();
	// Initialize the refraction render to texture object.
	result = refractionTexture->Initialize(context_.d3dDevice_, ContextWrapper::WIDTH, ContextWrapper::HEIGHT);
	if (!result)
	{
		std::cout << "Could not initialize the refraction render to texture object.";
		return S_FALSE;
	}

	// Create the reflection render to texture object.
	reflectionTexture = new RenderTexture();

	// Initialize the reflection render to texture object.
	result = reflectionTexture->Initialize(context_.d3dDevice_, ContextWrapper::WIDTH, ContextWrapper::HEIGHT);
	if (!result)
	{
		std::cout << "Could not initialize the reflection render to texture object.";
		return S_FALSE;
	}

	refractionShader = new RefractionShader();
	result = refractionShader->Initialize(context_.d3dDevice_);
	if (!result)
	{
		std::cout << "Could not initialize the refraction shader object.";
		return S_FALSE;
	}

	//Prepare Water Shader
	waterShader = new WaterShaderClass();
	result = waterShader->Initialize(context_.d3dDevice_);
	if (!result)
	{
		std::cout << "Could not initialize the refraction shader object.";
		return S_FALSE;
	}
	return S_OK;
}
// ===========================================
// Load shaders
// ===========================================
bool WaterCausticsShadingExample::reloadShadersInternal()
{
	// Define the input layout
	std::vector<D3D11_INPUT_ELEMENT_DESC> texturedLayout = {
	   { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	   { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	   { "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	   { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	return Shaders::makeShader<TextureShader>(texturedPhong_, context_.d3dDevice_, 
		L"shaders/Textured.fx", "VS", L"shaders/Textured.fx", "PS", texturedLayout);
}
// ===========================================
// Main render loop
// ===========================================
void WaterCausticsShadingExample::render()
{
	BaseExample::render();

	context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, Util::srgbToLinear(DirectX::Colors::MidnightBlue));
	context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// following line just put there for the sake of compilation
	XMFLOAT4 sunPos = XMFLOAT4(-30.0f, 30.0f, -30.0f, 1.0f);

	//Morris commented this
	/*
	// ==========
	// Draw floor
	// ==========
	XMFLOAT4 sunPos = XMFLOAT4(-30.0f, 30.0f, -30.0f, 1.0f);

	Texturing::ConstantBuffer cb;
	cb.World = XMMatrixIdentity();
	cb.NormalMatrix = computeNormalMatrix(cb.World);
	cb.Projection = XMMatrixTranspose(projection_);
	cb.View = XMMatrixTranspose(camera_.getViewMatrix());
	cb.ViewPos = camera_.Position;
	cb.DirLightCount = 1;
	cb.DirLights[0].Color = SUN_YELLOW;
	cb.DirLights[0].Direction = XMFLOAT4(-sunPos.x, -sunPos.y, -sunPos.z, 1.0f);

	texturedPhong_->updateConstantBuffer(context_.immediateContext_, cb);
	texturedPhong_->use(context_.immediateContext_);

	XMFLOAT4 planePos = XMFLOAT4(0.0, -2.0f, 0.0f, 1.0f);
	const XMMATRIX planeScale = XMMatrixScaling(20.0f, 0.2f, 20.0f);
	cb.World = XMMatrixTranspose(planeScale * XMMatrixTranslationFromVector(XMLoadFloat4(&planePos)));
	cb.NormalMatrix = computeNormalMatrix(cb.World);

	texturedPhong_->updateConstantBuffer(context_.immediateContext_, cb);
	context_.immediateContext_->PSSetShaderResources(0, 1, &groundFloorTexture_);
	context_.immediateContext_->PSSetSamplers(0, 1, &textureSampler_);
	groundPlane_->draw(context_.immediateContext_);
	*/

	// ==========
	// Draw sun
	// ==========
	ConstantBuffers::SolidConstBuffer scb;
	scb.OutputColor = SUN_YELLOW;
	scb.Projection = XMMatrixTranspose(projection_);
	scb.View = XMMatrixTranspose(camera_.getViewMatrix());
	const XMMATRIX scale = XMMatrixScaling(0.2f, 0.2f, 0.2f);
	scb.World = XMMatrixTranspose(scale * XMMatrixTranslationFromVector(XMLoadFloat4(&sunPos)));

	solidShader_->updateConstantBuffer(context_.immediateContext_, scb);
	solidShader_->use(context_.immediateContext_);
	//colorCube_->draw(context_.immediateContext_);

	context_.swapChain_->Present(0, 0);

	//Draw water
	// Put the water model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	waterModel->Render(context_.immediateContext_);

	waterTranslation += 0.001f;
	if (waterTranslation > 1.0f)
	{
		waterTranslation -= 1.0f;
	}

	//reflectionMatrix = m_Camera->GetReflectionViewMatrix();

	// Render the water model using the water shader.
	//result = waterShader->Render(context_.immediateContext_, waterModel->GetIndexCount(), cb.World, camera_.getViewMatrix(),
	//	projection_, reflectionMatrix, reflectionTexture->GetShaderResourceView(),
	//	m_RefractionTexture->GetShaderResourceView(), waterModel->GetTexture(),
	//	waterTranslation, 0.01f);
}
// ===========================================
// Handle user input
// ===========================================
void WaterCausticsShadingExample::handleInput()
{
	BaseExample::handleInput();
}

/*
bool WaterCausticsShadingExample::RenderRefractionToTexture()
{
	XMFLOAT4 clipPlane;
	XMMATRIX worldMatrix, viewMatrix, projectionMatrix;
	bool result;


	// Setup a clipping plane based on the height of the water to clip everything above it.
	clipPlane = XMFLOAT4(0.0f, -1.0f, 0.0f, waterHeight + 0.1f);

	// Set the render target to be the refraction render to texture.
	refractionTexture->SetRenderTarget(context_, context_->depthStencil_);

	// Clear the refraction render to texture.
	refractionTexture->ClearRenderTarget(context_, context_->depthStencil_, 0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	//m_Camera->Render();

	// Get the world, view, and projection matrices from the camera and d3d objects.
	
	//TODO:
	//m_D3D->GetWorldMatrix(worldMatrix);
	XMFLOAT4 planePos = XMFLOAT4(0.0, -2.0f, 2.0f, 1.0f); //We set the water at this coordonates
	const XMMATRIX planeScale = XMMatrixScaling(20.0f, 0.2f, 20.0f); //We scale by this value
	worldMatrix = XMMatrixTranspose(planeScale * XMMatrixTranslationFromVector(XMLoadFloat4(&planePos)));

	viewMatrix = camera_.getViewMatrix(); //->GetViewMatrix(viewMatrix);
	projectionMatrix = projection_;
	//m_D3D->GetProjectionMatrix(projectionMatrix);

	// Translate to where the bath model will be rendered.
	//D3DXMatrixTranslation(&worldMatrix, 0.0f, 2.0f, 0.0f);
	worldMatrix = XMMatrixTranslation(0.0f, 2.0f, 0.0f);

	// Put the bath model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	//m_BathModel->Render(m_D3D->GetDeviceContext());

	// Render the bath model using the light shader.
	result = refractionShader->Render(context_, m_BathModel->GetIndexCount(), worldMatrix, viewMatrix,
		projectionMatrix, m_BathModel->GetTexture(), m_Light->GetDirection(),
		m_Light->GetAmbientColor(), m_Light->GetDiffuseColor(), clipPlane);
	if (!result)
	{
		return false;
	}

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	//m_D3D->SetBackBufferRenderTarget();
	context_->OMSetRenderTargets(1, &m_renderTargetView, context_->depthStencil_);

	return true;
}


bool WaterCausticsShadingExample::RenderReflectionToTexture()
{
	XMMATRIX reflectionViewMatrix, worldMatrix, projectionMatrix;
	bool result;

	// Set the render target to be the reflection render to texture.
	m_ReflectionTexture->SetRenderTarget(m_D3D->GetDeviceContext(), context_->depthStencil_);

	// Clear the reflection render to texture.
	m_ReflectionTexture->ClearRenderTarget(m_D3D->GetDeviceContext(), m_D3D->GetDepthStencilView(), 0.0f, 0.0f, 0.0f, 1.0f);

	// Use the camera to render the reflection and create a reflection view matrix.
	m_Camera->RenderReflection(m_waterHeight);

	// Get the camera reflection view matrix instead of the normal view matrix.
	reflectionViewMatrix = m_Camera->GetReflectionViewMatrix();

	// Get the world and projection matrices from the d3d object.
	m_D3D->GetWorldMatrix(worldMatrix);
	m_D3D->GetProjectionMatrix(projectionMatrix);

	// Translate to where the wall model will be rendered.
	D3DXMatrixTranslation(&worldMatrix, 0.0f, 6.0f, 8.0f);

	// Put the wall model vertex and index buffers on the graphics pipeline to prepare them for drawing.
	m_WallModel->Render(m_D3D->GetDeviceContext());

	// Render the wall model using the light shader and the reflection view matrix.
	result = m_LightShader->Render(m_D3D->GetDeviceContext(), m_WallModel->GetIndexCount(), worldMatrix, reflectionViewMatrix,
		projectionMatrix, m_WallModel->GetTexture(), m_Light->GetDirection(),
		m_Light->GetAmbientColor(), m_Light->GetDiffuseColor());
	if (!result)
	{
		return false;
	}

	// Reset the render target back to the original back buffer and not the render to texture anymore.
	m_D3D->SetBackBufferRenderTarget();

	return true;
}
*/

} // end namespace
