# Rendering Water Caustics (CGFCG_WaterCaustics)

This project represents a semester project for the course "Computer Graphics for Game Development", taught at Charles University, Prague.
More information on the course can be found [here](https://gamedev.cuni.cz/study/courses-history/courses-2019-2020/computer-graphics-for-computer-games-summer-201920/).

If you want, see our [Trello Board](https://trello.com/b/VDJ4leFX/cgfcgwatercaustics) for the project.

## Overall goal of this project and what we wish to achieve with it

The goal of the project is to deliver a DirectX 11 version of a shader showcasing the effects caused by water caustics in games.
We aim for creating a simulation for realistic caustics animation (in contrast to e.g. randomly moving Voronoi patterns).
For the overall appearance of the water caustics, we aim for a "visually pleasing look", which not necessarily needs to be physically plausible.
Optionally, we would like to work with an already existing, free-to-use implementation of water surface movements, in order to visualize the resulting caustics from varying wave heights and depths and their movement.

## Target result of the project

The expected result is a DirectX 11 shader which will simulate the water caustics in an almost real-life like way. In addition, the program will allow the user a bit of interactivity - mainly camera movement.

## State of the art

- [Working With Ray Traced Water Caustics in DXR (YouTube video)](https://www.youtube.com/watch?v=l-wTZLjhZ5Y&t=20s)
- [Ray Tracing Gems - Ray-Guided Volumetric Water Caustics in Single Scattering Media with DXR](https://link.springer.com/chapter/10.1007/978-1-4842-4427-2_14)
- [Efficient Caustic Rendering with Lightweight Photon Mapping](https://cgg.mff.cuni.cz/~jaroslav/papers/2018-lwpm/index.htm) (general)
- [Real-Time Mixed Reality Rendering for Underwater 360° Videos](https://www.wgtn.ac.nz/__data/assets/pdf_file/0011/1771868/ismar2019-paper-real-time-mixed-reality-rendering-for-underwater-360-videos.pdf)
- [Believable Caustics Reflections](https://www.alanzucconi.com/2019/09/13/believable-caustics-reflections/)

## Initial state
##### Sebastian: 
- basic C++ programming knowledge
- theoretical knowledge about computer graphics topics thorough the lectures at 
- no experience at GPU programming 

##### Morris:    
- medium C++ programming knowledge
- experienced in game development (e.g. through courses, Ludum Dares)
- no experience with shading languages

##### The project
- will be written in C++ using DirectX 11
- will start from the blank template of a DirectX 11 project

## Technologies, libraries and frameworks used

We will be working with Visual Studio on this project, using DirextX 11.
The framework we are using is provided by our university and can be found [here](https://github.com/Aldarrion/DX11Examples).

\[This section may be extended in the future.\]

## Features of this project

- Simulated water
- Caustics shader
- Input controls (camera movement)
- Movable light sources

## Planning

0. Main research on the topic at hand
1. Integration with the starting template of DirectX 11
2. Setting up and configuring the main necessities (window, input, camera, etc)
3. Adding main elements for the showcase (simulated water, 3D objects, light sources)
4. Trial and error on the caustics shader for the water
5. Finishing the caustics shader for the water

\[This steps are subject to change\]